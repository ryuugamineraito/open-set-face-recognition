import os
import numpy as np
import pandas as pd
import shutil
import pickle
base_dir = 'CASIA-WebFace_cropped'
image_folder = np.array(os.listdir(base_dir))
np.random.seed(0)
np.random.shuffle(image_folder)
cand_known_class = image_folder[:1000]
unknown_class = image_folder[1000:2000]

# print(len(cand_known_class))
# print(len(unknown_class))
limit_known_class = 100
count_known=0
class_count = 0
known_class = []
list_train_dir = []
list_test_known_dir=[]
list_test_unknown_dir = []
train_df = pd.DataFrame()
test_df = pd.DataFrame()
####### create train.csv########################
for i in cand_known_class:
    dir = os.path.join(base_dir,i)
    if len(os.listdir(dir))>80 and len(known_class)<limit_known_class:
        file_dir = [os.path.join(dir,i) for i in os.listdir(dir)]
        folder_list =np.array(file_dir)
        np.random.shuffle(folder_list)
        known_class.append(i)
        list_train_dir.extend(folder_list[:70])
        list_test_known_dir.extend(folder_list[70:80])


train_df['images'] = list_train_dir
train_df['labels'] = [i.split('/')[-2] for i in list_train_dir]
train_df.to_csv('train.csv',index=False)
# print(train_df)
################ create test.csv ######################
for i in unknown_class:
    dir = os.path.join(base_dir,i)
    file_dir = np.array(os.listdir(dir))
    np.random.shuffle(file_dir)
    source_file = os.path.join(dir,file_dir[0])
    list_test_unknown_dir.append(source_file)
list_test_known_label = [i.split('/')[-2] for i in list_test_known_dir]
list_test_unknown_label = [0] * len(list_test_unknown_dir)
test_df['images'] = [*list_test_known_dir,*list_test_unknown_dir]
test_df['labels'] = [*list_test_known_label,*list_test_unknown_label]
test_df.to_csv('test.csv',index=False)

############## create cls_to_idx dictionary ###############
known_class.sort()
cls_to_idx = {value:key+1 for key,value in enumerate(known_class)}
cls_to_idx['0'] = 0
with open('cls_to_idx.pkl','wb') as f:
    pickle.dump(cls_to_idx, f)